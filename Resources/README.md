# Read me before doing anything

[Official FIRST resources page](https://www.firstinspires.org/resource-library/ftc/game-and-season-info).

This contains resources for programming the FTC bot.

First, view the PDF file.

Then, refernce the [FTC programming wiki](https://github.com/ftctechnh/ftc_app/wiki) to figure out what you're supposed to do.

We've forked the FTC app code to [here](https://github.com/NBPSEaglebotics/ftc_app). You should use that over the main repository (we will keep them in sync).

There are 3 different ways to code the bot:

1. [FTC Blocks](https://github.com/ftctechnh/ftc_app/wiki/Blocks-Tutorial)
2. [OnBot Java](https://github.com/ftctechnh/ftc_app/wiki/OnBot-Java-Tutorial)
3. Android Studio (see PDF)

