# FTC Relic Recovery (2017-18)
This repository contains the game’s rules.

## The Game

[Watch the video](https://youtu.be/7Wc1LhG2FEs). This portion covers what the video doesn't.

### General
* A *cryptobox key* is a single column of the cryptobox, the identity of which is encoded into the pictographs. They count for more points if scored during the auto period.

### Pictographs
See Appendix B of the [game manual](https://firstinspiresst01.blob.core.windows.net/ftc/game-manual-dw-part-2.pdf).

## Tournament
### General Rules

* **No WiFi or wireless communications allowed** (except legal communication between Drive Team and Robot)
	* WiFi Hotspots are banned, **radios/walkie talkies are banned**, etc.
	* **If you’re caught breaking this rule, the team will be literally kicked out of the event.**
* Teams receive a minimum of 7 minutes between matches

### Yellow and Red Cards

* **A yellow card is a warning, whereas a red card will disqualify your team.** 
* *Two consecutive yellow cards will turn into one red card.*
* You can receive a yellow or red card anywhere during the event.
* Yellow cards don’t carry over from Qualifying matches to Elimination matches.

### Questioning a Match or Score
* Located in the Referee Question Box
* One student from the alliance is allowed to enter.
* Questions regarding Qualifiers can be asked *before 3 matches have passed* since the match in question.
* Questions regarding Elimination matches can be asked *before the start of the next match played by the alliance.*

## The Robot
### Control System

* *One Android device acts as Robot Controller*
* *One Android acts as a Driver Station when connected to gamepads*

### Important Technology Definitions

There are two main manufacturers of robotics parts: Modern Robotics and REV Robotics. 

* **Modern Robotics Core Control Modules**
	* *Core Device Interface Module*: provides input and output (I/O) for the Robot Controller
		* 8 digital input and output
		* 8 analog input
		* 2 analog output
		* 2 PWM output
		* 6 high-speed (100kHz) I^2C ports
	* *Core Motor Controller*: A USB-enabled DC motor controller with 2 channels.
	* *Core Power Distribution Module*: Connects Robot Controller to multiple USB-enabled modules and draws power from a FIRST approved battery (see Rules section)
		* e.g. Core Motor Controller, Core Servo Controller
	* *Core Servo Controller*: USB-enabled servo controller with 6 channels
* *Driver Station*: Used to send signals to the Robot Controller
	* Android device with FTC supplied app
	* gamepads
		* Logitech F310 Gamepad or Xbox 360 Controller for Windows
	* optional: Android device can be hooked up to external battery charger
* **REV Robotics Parts**
	* *Expansion Hub*: 
		* 4 DC motor channels
		* 6 servo channels
		* 8 input/output
		* 4 analog input channels
		* 4 independent I^2C Buses
		* Can be connected to a second hub
	* *Servo Power Module*: boosts power given to 3-wire servos. Has 6 servo I/O ports. Draws 12V and gives 6V to each connected servo. Gives 15 amps of current across all output servo ports for 90 watts per module.
	* *Logic Level Converter*: allows an encoder or sensor using 5V logic levels to work with the Expansion Hub (which uses 3.3V logic)
	* *I^2C Sensor Adapter Cable*: changes pin orientation of to match a Modern Robotics I^2 sensor.
* **Approved Batteries**
	* TETRIX (W3905, formally 739023) 12 VDC Battery Pack
	* Modern Robotics/MATRIX (14-0014) 12 VDC Battery Pack
	* REV Robotics (REV-31-1302) 12 VDC Slim Battery Pack
	* **Legacy systems have other requirements; see Manual Part 1, section 8.3.3.**
* Approved Android Devices
	* ZTE Speed (needs special app for WiFi Direct: ["Wi-Fi Direct Channel Changing"](https://play.google.com/store/apps/details?id=com.zte.wifichanneleditor))
	* Moto G Gen 2 or Gen 3
	* Moto G4 Play
	* Nexus 5
	* Galaxy S5
### Rules and Restrictions

* **Anything that can harm the playing field is banned.**
* **The robot must be a maximum size of 18 by 18 by 18 (inches).**
	* *exceptions*: alliance flag, preloaded game elements
* *Robot Controller should be visible and accessible to FIRST volunteers.* If your bot needs to be serviced, this will make it easier for them.
* **The robot must have a mounting device for the alliance flag.**
	* e.g. soda straw, wooden dowel
* **Robot must display its team number, one on each side.**
	* use Arial, Bold, 250 point to meet requirements
* *Parts of the robot may not be launched, even if the launched part remains connected via tether.*
	* *Scoring elements can be launched, but only if they don’t cause safety issues.*
* *Raw and processed materials may be used given they are from a vendor which is accessible to majority of teams (e.g. Home Depot, AndyMark)*
* *Commercial Off-The-Shelf mechanisms may be used if they have a single degree of freedom (motion defined by one co-ordinate/function).*
* 3D printing is allowed
* **Batteries must be securely fastened and cannot touch the field or another bot.**
* **A power switch is required. The approved battery vendors also provide corresponding switches.**
* **All fuses must be single-use only (no breakers).**
* The Robot controller must be powered by its own *internal* battery or by the REV Expansion Hub.
* A maximum of 8 DC motors are allowed.
* A maximum of 12 servos are allowed.
* Light sources (e.g. LEDs) cannot be focused/directed in any way
* Video cameras are allowed on the bot if they are **only** used for post-match viewing and the WiFi is turned off
* Electrically grounding electronics to the bot's frame is not allowed
* Electronics cannot be modified internally or in some way which affects their safety

### Software Rules
* Robot Controller must be named with the team number plus "-RC" (e.g. 5410-RC)
* Minimum allowed version of Java is 3.1
* Programming can be done with any of the following:
	* FTC Blocks Programming
	* Android Studio
	* MIT App Inventor
	* Java Native Interface and Android Native Development Kit
	* FTC OnBot Java Programming
* Allowed Android OSes
	* ZTE Speed: 4.4 + (Kit-Kat)
	* Moto G Gen 2 and 3, Nexus 5, and Galaxy S5: 6.0 + (Marshmellow)
	* Moto G4 Play: 6.0.1 + (Marshmellow)
* Robot Controller must have the [Robot Controller](https://play.google.com/store/apps/details?id=com.qualcomm.ftcrobotcontroller) App.
* Driver Station must have the [Driver Station](https://play.google.com/store/apps/details?id=com.qualcomm.ftcdriverstation) App.
* Androids must be set to Airplane mode with Bluetooth turned off.
